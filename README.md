##Otalkoo

**Otalkoo** is a chat webplatform which allows people to work together without emails.

Otalkoo is made of *Rooms*. Rooms can be available to everyone or to a specific group of people (private rooms). Within each room, one or more *Conversations* can be created. To participate to a conversation it is necessary to enter in the room and subscribe to the conversation. After that, the only thing is necessary to do is write a message and start talking with other registered people.

Rooms are contained within *Spheres* which are like Chat spaces for a company or an organization. Only sphere users can access to its rooms. In this way it's very easy to handle a dedicated chat space within a company, a department, a classroom, ...

To access the application it is required to be registered. User must signup by creating a new account (name, firstname, email and password). Once user is logged in, he can access to this homepage and start by joining an existing sphere or send a request to join. Furthermore, it's possible for a sphere creator to invit people to join its own sphere. Once the user is logged in, he can access to the sphere page which contains two lists : Public and Privates rooms.

####FEATURES

+ Spheres : allows to manage a chat environment for companies or organization. **NEW**
+ Manage users within the sphere and send invitation to guests (outside of the company domain), i.e : customers, extras, ...
+ Public rooms available by everyone within a sphere
+ Private rooms by creator's room and invited people
+ Display who is already connected
+ Users control at the level of room and conversation if the privacy is set up to Private
+ Notifications system (by considering the users subscriptions)

####TECHNOLOGIES

+ Meteor (with the power of Socket.io)
+ Twitter Bootstrap 3 (Less)
+ CoffeeScript