Router.configure 
  layoutTemplate: 'layout',
  loadingTemplate: 'loading'

Router.map () -> 
  # Root
  @.route 'root',
    path: '/'
    onBeforeAction: () ->
      if Meteor.loggingIn()
        Router.go('home')

  # Home
  @.route 'home',
    path: '/home/'
    waitOn: () ->
      Meteor.subscribe('sphere')
      Meteor.subscribe('publicRooms')
      Meteor.subscribe('privateRoom')
      Meteor.subscribe('usersToInvit')

  # RoomPage
  @.route 'roomPage',
    path: '/rooms/:_id'
    waitOn: () ->
      Meteor.subscribe('roomDetails', @.params._id)
      Meteor.subscribe('conversations', @.params._id)
      Meteor.subscribe('usersToInvit')
    data: () -> 
      Rooms.findOne(@.params._id)

  # ConversationPage
  @.route 'conversationPage',
    path: '/rooms/:roomId/conversations/:_id'
    waitOn: () ->
      Meteor.subscribe('roomDetails', @.params.roomId)
      Meteor.subscribe('conversationDetails', @.params._id)
      Meteor.subscribe('usersToInvit')
      Meteor.subscribe('posts', @.params._id)
    data: () ->
      _.extend(Conversations.findOne(@.params._id), {'isConversationPage': true})

requireLogin = () -> 
  if not Meteor.user()
    if Meteor.loggingIn()
      @.render(this.loadingTemplate)
    else
      throwError('Warning ! You need to be logged in !')
      Router.go('root')

Router.onBeforeAction(requireLogin, {only: ['home', 'roomPage', 'conversationPage']})
Router.onBeforeAction -> clearErrors()