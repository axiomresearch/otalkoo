Template.errors.helpers
  errors: () ->
    Errors.find()

Template.error.rendered = () ->
  Meteor.setTimeout(
    Errors.update(@.data._id, {$set: {seen: true}}) if @data isnt undefined, 
    1
  )