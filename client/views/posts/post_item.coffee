Template.postItem.helpers 
  fromNow: () ->
    moment(@.created_date).fromNow()
  owner: () ->
    @.userId is Meteor.userId()