Template.conversationDetails.helpers 
  owner: () ->
    @userId is Meteor.userId()
  fromNow: () ->
    moment(@created_date).fromNow()
  hasTags: () ->
    @.tags isnt undefined
  connected: () ->
    @.users.length if @.users isnt undefined
  plural: () ->
    @.users.length > 1 if @.users isnt undefined

Template.conversationDetails.events
  'click #conversation-edit': (e) ->
    e.preventDefault()

    conversationId = $(e.target).data('id')
    conversation = Conversations.findOne({_id: conversationId})

    # Set Session variable with tags list
    Session.set('conversationTags', conversation.tags)

    # Load form within modal and display it
    instance = UI.renderWithData(Template.conversationEdit, conversation)
    UI.insert(instance, $('.modal .modal-content')[0])

    $('#scriptchat-modal').modal 'show'

  'click #conversation-management': (e) ->
    e.preventDefault()

    conversationId = $(e.target).data('id')
    conversation = Conversations.findOne({_id: conversationId})

    # Set Session variable with users list
    Session.set('conversationUsers', _.without(conversation.users, Meteor.user().username))

    # Load form within modal and display it
    instance = UI.renderWithData(Template.conversationManagement, conversation)
    UI.insert(instance, $('.modal .modal-content')[0])

    $('#scriptchat-modal').modal 'show'

  'click #conversation-delete': (e) ->
    e.preventDefault()

    conversationId = $(e.target).data('id')

    # Load form within modal and display it
    instance = UI.renderWithData(Template.conversationDelete, Conversations.findOne({_id: conversationId}))
    UI.insert(instance, $('.modal .modal-content')[0])

    $('#scriptchat-modal').modal 'show'