Template.conversationItem.helpers 
  fromNow: () ->
    moment(@.created_date).fromNow()
  connected: () ->
    @.users.length
  plural: () ->
    @.users.length > 1

Template.conversationItem.events
  'click .conversation-play': (e) ->
    e.preventDefault()

    # Get conversation Id
    conversation = $(e.currentTarget).parent()
    conversationId = conversation.data('id')

    # Get previous selected item if it exists and
    # update CSS by removing class
    previousConversationId = Session.get('currentConversationId')

    if previousConversationId isnt undefined
      conversation.parent().find('[data-id=\'' + previousConversationId + '\']').removeClass('selected')

    # Set conversationId into Session variable
    Session.set('currentConversationId', conversationId)

    # Update CSS for new selected item
    conversation.addClass('selected')

    # Get conversation data
    data = Conversations.findOne({_id: Session.get('currentConversationId')})

    # Load data within template and display it
    $('#item-description').empty()
    instance = UI.renderWithData(Template.conversationDetails, data)
    UI.insert(instance, $('#item-description')[0])

    # Update offset of the conversation details block 
    # from the position of the target element.
    $('.conversation-details-container').offset
      top: conversation.offset().top - conversation.height()