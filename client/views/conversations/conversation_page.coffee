Template.conversationPage.helpers 
  posts: () ->
    Posts.find({conversationId: @._id})
  roomName: () ->
    Rooms.findOne({_id: @.roomId}).name if @.roomId isnt undefined
  fromNow: () ->
    moment(@.created_date).fromNow()
  connected: () ->
    @.users.length if @.users isnt undefined
  plural: () ->
    @.users.length > 1 if @.users isnt undefined

Template.conversationCreate.events
  # Send a new post
  'submit #post-new': (e) ->
    e.preventDefault()

    # Check if post content is not empty
    msg = $(e.target).find('[name=post]').val()

    if msg.length > 1
      post =
        conversationId: $(e.target).data('conversation-id')
        message: msg

      Meteor.call 'sendPost', post, (error, id) -> 
        if error
          throwError(error.reason)
    else
        throwError('Warning ! Your post must not be empty !')

Template.conversationPage.rendered = () ->
  # Set focus on post-sender field
  $('.post-sender-field').focus()

  # Send a new post
  $('#send-post').on 'keypress', (e) ->
    if e.which is 13
      e.preventDefault()

      # Check if post content is not empty
      msg = $(e.target).val()

      if msg.length > 1
        post =
          conversationId: $(e.target).parent().parent().data('conversation-id')
          message: msg

        Meteor.call 'sendPost', post, (error, id) -> 
          if error
            throwError(error.reason)
          else
            $(e.target).val('')
      else
        throwError('Warning ! Your post must not be empty !')