# Utitlity methods
renderConversationDetails = (conversationId) ->
  # Get conversation data
  data = Conversations.findOne({_id: conversationId})

  # Refresh conversation details template
  position = $('.conversation-details-container').offset().top

  $('#item-description').empty()
  instance = UI.renderWithData(Template.conversationDetails, data)
  UI.insert(instance, $('#item-description')[0])

  # Update offset of the conversation details block.
  $('.conversation-details-container').offset
    top: position

# Conversation EDIT
Template.conversationEdit.helpers
  tags: () ->
    Session.get('conversationTags')

Template.conversationEdit.rendered = () ->
  $('input[name="name"]').focus()

  $('.btn-privacy').on 'click', (e) ->
    if $(e.target).find('[name=is_private]').val() is 'true'
      $('.privacy-alert').text('WARNING ! FROM NOW YOU MUST MANAGE USERS WHICH CAN ACCESS TO THIS CONVERSATION')
    else
      $('.privacy-alert').empty()

  $('#conversation-privacy-infos').popover
     container: '.modal' 
     toggle: 'popover' 
     placement: 'right' 
     content: 'Limit the access to your conversation only for invited users. All users which are not in the user list will lost their access to this conversation !'
     trigger: 'hover'

  $('#add-new-tag').on 'keydown', (e) ->
    if e.which is 13
      e.preventDefault()

      # Push new tag item into Session array
      tags = Session.get('conversationTags')
      tags.push($(e.target).val().toUpperCase())
      Session.set('conversationTags', tags)
      $(e.target).val('')

Template.conversationEdit.events
  'click .delete-tag': (e) ->
    e.preventDefault()

    tags = Session.get('conversationTags')
    new_tags = _.difference(tags, $(e.target).data('name'))
    Session.set('conversationTags', new_tags)

  # Edit a conversation
  'submit #conversation-edit': (e) ->
    e.preventDefault()

    conversationId = $(e.target).data('conversation-id')
    newName = $(e.target).find('[name=name]').val()
    tags = Session.get('conversationTags')

    # Define privacy
    if $(e.target).find('.active [name=is_private]').length > 0
      is_private = $(e.target).find('.active [name=is_private]').val() is 'true'
    else
      is_private = false

    Conversations.update conversationId,
      $set:
        name: newName
        is_private: is_private
        tags: tags
      (error) ->
        if error
          throwError(error.reason)

        # Render conversationDetails again
        renderConversationDetails(conversationId)
        
        $('#scriptchat-modal').modal 'hide'

# Conversation MGMT
Template.conversationManagement.helpers
  invited: () ->
    Session.get('conversationUsers')

Template.conversationManagement.settings = () ->
  position: "bottom"
  limit: 5
  rules: [
    token: '@'
    collection: Meteor.users
    field: "username"
    filter: {'_id': {'$not': Meteor.userId()}, 'username': {'$nin': Session.get('conversationUsers')}}
    template: Template.userAutoComplete
    noMatchTemplate: Template.userAutoCompleteNotFound
  ]

Template.conversationManagement.events
  'click .delete-user': (e) ->
    e.preventDefault()

    users = Session.get('conversationUsers')
    new_users = _.difference(users, $(e.target).data('name'))
    Session.set('conversationUsers', new_users)

  'submit #conversation-management': (e) ->
    e.preventDefault()

    conversationId = $(e.target).data('id')

    Conversations.update conversationId,
      $set:
        users: _.union(Meteor.user().username, Session.get('conversationUsers'))
      (error) ->
        if error
          throwError(error.reason)

        # Render conversationDetails again
        renderConversationDetails(conversationId)
        
        $('#scriptchat-modal').modal 'hide'

Template.conversationManagement.rendered = () ->
  $('#invite-user').on 'keypress', (e) ->
    if e.which is 13
      e.preventDefault()

      # Split result if multiple users
      data = $.map _.rest($(e.target).val().split('@')),
        (item) -> item.trim()

      # Append name to the invitedUsers array after
      # process a unique treatment
      new_data = _.uniq(_.union(Session.get('conversationUsers'), data))

      # Init the input
      $(e.target).val('@')

      Session.set('conversationUsers', new_data)

# Conversation DELETE
Template.conversationDelete.events
  # Delete a conversation
  'submit #conversation-delete': (e) ->
    e.preventDefault()

    conversationId = $(e.target).data('id')
    Conversations.remove conversationId
        
    $('#item-description').empty()
    $('#scriptchat-modal').modal 'hide'