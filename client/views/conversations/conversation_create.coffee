Template.conversationCreate.helpers 
  invited: () ->
    Session.get('conversationUsers')
  tags: () ->
    Session.get('conversationTags')

Template.conversationCreate.settings = () ->
  position: "bottom"
  limit: 5
  rules: [
    token: '@'
    collection: Meteor.users
    field: "username"
    filter: {'_id': {'$not': Meteor.userId()}, 'username': {'$nin': Session.get('conversationUsers')}}
    template: Template.userAutoComplete
    noMatchTemplate: Template.userAutoCompleteNotFound
  ]

Template.conversationCreate.events
  'click .delete-user': (e) ->
    e.preventDefault()

    users = Session.get('conversationUsers')
    new_users = _.difference(users, $(e.target).data('name'))
    Session.set('conversationUsers', new_users)

  'click .delete-tag': (e) ->
    e.preventDefault()

    tags = Session.get('conversationTags')
    new_tags = _.difference(tags, $(e.target).data('name'))
    Session.set('conversationTags', new_tags)

  # Create new conversation
  'submit #conversation-new': (e) ->
    e.preventDefault()

    # Define privacy
    if $(e.target).data('is-private')
      is_private = $(e.target).find('.active [name=is_private]').val() is 'true'
    else
      # Default behavior
      is_private = false

    conversation =
      roomId: $(e.target).data('room-id')
      parent_is_private: Rooms.findOne({_id: $(e.target).data('room-id')}).is_private
      name: $(e.target).find('[name=name]').val(),
      is_private: is_private,
      users: Session.get('conversationUsers'),
      tags: Session.get('conversationTags')

    Meteor.call 'createConversation', conversation, (error, id) -> 
      if error
        throwError(error.reason)
      $('#scriptchat-modal').modal 'hide'

Template.conversationCreate.rendered = () ->
  $('#conversation-privacy-infos').popover
     container: '.modal' 
     toggle: 'popover' 
     placement: 'right' 
     content: 'Limit the access to your conversation only for invited users'
     trigger: 'hover'

  $('#invite-user').on 'keypress', (e) ->
    if e.which is 13
      e.preventDefault()

      # Split result if multiple users
      data = $.map _.rest($(e.target).val().split('@')),
        (item) -> item.trim()

      # Append name to the conversationUsers array after
      # process a unique treatment
      new_data = _.uniq(_.union(Session.get('conversationUsers'), data))

      # Init the input
      $(e.target).val('@')

      Session.set('conversationUsers', new_data)

  $('#add-new-tag').on 'keydown', (e) ->
    if e.which is 13
      e.preventDefault()

      # Push new tag item into Session array
      tags = Session.get('conversationTags')
      tags.push($(e.target).val().toUpperCase())
      Session.set('conversationTags', tags)
      $(e.target).val('')