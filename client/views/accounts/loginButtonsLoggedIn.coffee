Template._loginButtonsLoggedInDropdown.events
  'click #login-buttons-edit-profile': (event) ->
    event.stopPropagation()
    Template._loginButtons.toggleDropdown()
    Router.go('profileEdit')

Template._loginButtons.events
  'click #login-buttons-logout': () ->
      Meteor.logout () ->
        clearErrors()
        loginButtonsSession.closeDropdown() if loginButtonsSession isnt undefined