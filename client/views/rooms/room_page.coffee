Template.roomPage.helpers 
  conversations: () ->
    Conversations.find({roomId: @._id})
  fromNow: () ->
    moment(@.created_date).fromNow()

Template.roomPage.events
  'click .new-conversation': (e) ->
    e.preventDefault()

    # Empty peopleToInvit and Tags collection
    Session.set('conversationUsers', [])
    Session.set('conversationTags', [])

    data = 
      roomId: $(e.target).data('room-id')
      is_private: $(e.target).data('is-private')
      people: Session.get('conversationUsers')
      tags: Session.get('conversationTags') 

    # Load form within modal and display it
    instance = UI.renderWithData(Template.conversationCreate, data)
    UI.insert(instance, $('.modal .modal-content')[0])

    $('#scriptchat-modal').modal 'show'