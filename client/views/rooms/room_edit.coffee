# Room EDIT
Template.roomEdit.rendered = () ->
  $('input[name="name"]').focus()

Template.roomEdit.events
  # Edit a room
  'submit #room-edit': (e) ->
    e.preventDefault()

    roomId = $(e.target).data('id')
    newName = $(e.target).find('[name=name]').val()

    Rooms.update roomId,
      $set:
        name: newName
      (error) ->
        if error
          throwError(error.reason)
        
        $('#scriptchat-modal').modal 'hide'

# Room MGMT
Template.roomManagement.helpers
  invited: () ->
    Session.get('roomUsers')

Template.roomManagement.settings = () ->
  position: "bottom"
  limit: 5
  rules: [
    token: '@'
    collection: Meteor.users
    field: "username"
    filter: {'_id': {'$not': Meteor.userId()}, 'username': {'$nin': Session.get('conversationUsers')}}
    template: Template.userAutoComplete
    noMatchTemplate: Template.userAutoCompleteNotFound
  ]

Template.roomManagement.events
  'click .delete-user': (e) ->
    e.preventDefault()

    users = Session.get('roomUsers')
    new_users = _.difference(users, $(e.target).data('name'))
    Session.set('roomUsers', new_users)

  'submit #room-management': (e) ->
    e.preventDefault()

    roomId = $(e.target).data('id')

    Rooms.update roomId,
      $set:
        users: _.union(Meteor.user().username, Session.get('roomUsers'))
      (error) ->
        if error
          throwError(error.reason)
        
        $('#scriptchat-modal').modal 'hide'

Template.roomManagement.rendered = () ->
  $('#invite-user').on 'keypress', (e) ->
    if e.which is 13
      e.preventDefault()

      # Split result if multiple users
      data = $.map _.rest($(e.target).val().split('@')),
        (item) -> item.trim()

      # Append name to the invitedUsers array after
      # process a unique treatment
      new_data = _.uniq(_.union(Session.get('roomUsers'), data))

      # Init the input
      $(e.target).val('@')

      Session.set('roomUsers', new_data)

# Room DELETE
Template.roomDelete.events
  # Delete a room
  'submit #room-delete': (e) ->
    e.preventDefault()

    roomId = $(e.target).data('id')
    Rooms.remove roomId
        
    $('#scriptchat-modal').modal 'hide'