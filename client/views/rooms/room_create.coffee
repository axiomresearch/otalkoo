Template.roomCreate.helpers
  invited: () ->
    Session.get('invitedUsers')
  
Template.roomCreate.settings = () ->
  position: "bottom"
  limit: 5
  rules: [
    token: '@'
    collection: Meteor.users
    field: "username"
    filter: {'_id': {'$not': Meteor.userId()}, 'username': {'$nin': Session.get('conversationUsers')}}
    template: Template.userAutoComplete
    noMatchTemplate: Template.userAutoCompleteNotFound
  ]

Template.roomCreate.events
  'click .delete-user': (e) ->
    e.preventDefault()

    users = Session.get('invitedUsers')
    new_users = _.difference(users, $(e.target).data('name'))
    Session.set('invitedUsers', new_users)
  
  # Create new room
  'submit #room-new': (e) ->
    e.preventDefault()

    is_private = $(e.target).find('[name=is_private]').val() is 'true'

    room =
      name: $(e.target).find('[name=name]').val(),
      is_private: is_private

    if is_private
      room = _.extend(room, 
        users: Session.get('invitedUsers')
      )

    Meteor.call 'createRoom', room, (error, id) -> 
      if error
        throwError(error.reason)
      
      $('#scriptchat-modal').modal 'hide'

Template.roomCreate.rendered = () ->
  $('#invite-user').on 'keypress', (e) ->
    if e.which is 13
      e.preventDefault()

      # Split result if multiple users
      data = $.map _.rest($(e.target).val().split('@')),
        (item) -> item.trim()

      # Append name to the invitedUsers array after
      # process a unique treatment
      new_data = _.uniq(_.union(Session.get('invitedUsers'), data))

      # Init the input
      $(e.target).val('@')

      Session.set('invitedUsers', new_data)