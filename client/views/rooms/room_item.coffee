Template.roomItem.helpers 
  fromNow: () ->
    moment(@.created_date).fromNow()
  owner: () ->
    @userId is Meteor.userId()

Template.roomItem.events
  'click #room-edit': (e) ->
    e.preventDefault()

    roomId = $(e.target).data('id')

    # Load form within modal and display it
    instance = UI.renderWithData(Template.roomEdit, Rooms.findOne({_id: roomId}))
    UI.insert(instance, $('.modal .modal-content')[0])

    $('#scriptchat-modal').modal 'show'

  'click #room-management': (e) ->
    e.preventDefault()

    roomId = $(e.target).data('id')
    room = Rooms.findOne({_id: roomId})

    # Set Session variable with users list
    Session.set('roomUsers', _.without(room.users, Meteor.user().username))

    # Load form within modal and display it
    instance = UI.renderWithData(Template.roomManagement, room)
    UI.insert(instance, $('.modal .modal-content')[0])

    $('#scriptchat-modal').modal 'show'

  'click #room-delete': (e) ->
    e.preventDefault()

    roomId = $(e.target).data('id')

    # Load form within modal and display it
    instance = UI.renderWithData(Template.roomDelete, Rooms.findOne({_id: roomId}))
    UI.insert(instance, $('.modal .modal-content')[0])

    $('#scriptchat-modal').modal 'show'