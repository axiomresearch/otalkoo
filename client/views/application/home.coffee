Template.home.helpers
  sphere: () ->
    Spheres.find()
  publicRooms: () ->
    Rooms.find({is_private: false})
  privateRooms: () ->
    Rooms.find({is_private: true})

Template.home.events
  'click .new-item': (e) ->
    e.preventDefault()

    isPrivate = $(e.target).data('is-private')

    # Load form within modal and display it
    instance = UI.renderWithData(Template.roomCreate, 'is_private': isPrivate)
    UI.insert(instance, $('.modal .modal-content')[0])

    if isPrivate
      Session.set('invitedUsers', [])

    $('#scriptchat-modal').modal 'show'