Spheres = new Meteor.Collection('spheres');

Spheres.allow({
  update: ownsDocument,
  remove: ownsDocument
});

Spheres.deny({
  insert: function(userId, sphere, fieldNames) {
    // May only insert with the following fields:
    return (_.without(fieldNames, 'name', 'company', 'url', 'domain', 'managers', 'users', 'guests').length > 0);
  },
  update: function(userId, sphere, fieldNames) {
    // May only edit the following fields:
    return (_.without(fieldNames, 'name', 'company', 'url', 'domain', 'managers', 'users', 'guests').length > 0);
  }
});

Meteor.methods({
  createSphere: function(sphereAttributes) {
    var user = Meteor.user(),
      sphereWithSameName = Spheres.findOne({name: sphereAttributes.name}),
      sphereWithSameCompany = Spheres.findOne({company: sphereAttributes.company}),
      sphereWithSameDomain = Spheres.findOne({domain: sphereAttributes.domain}),
      sphereWithSameUrl = Spheres.findOne({url: sphereAttributes.url});

    // Ensure the user is logged in
    if (!user)
      throw new Meteor.Error(401, "You need to login to create new rooms");

    // Ensure the sphere has a name
    if (!sphereAttributes.name)
      throw new Meteor.Error(422, 'Please fill in a name for the sphere');

    // Check that there are no previous spheres with the same name
    if (sphereAttributes.name && sphereWithSameName) { 
      throw new Meteor.Error(302,
'This name has already been used'); }

    // Check that there are no previous spheres with the same company name
    if (sphereAttributes.company && sphereWithSameCompany) { 
      throw new Meteor.Error(302,
'This company name has already been used'); }

    // Check that there are no previous spheres with the same domain
    if (sphereAttributes.domain && sphereWithSameDomain) { 
      throw new Meteor.Error(302,
'This domain has already been used'); }

    // Check that there are no previous spheres with the same url
    if (sphereAttributes.url && sphereWithSameUrl) { 
      throw new Meteor.Error(302,
'This url has already been used'); }

    // Add owner as first manager for this new sphere
    interns = []
    interns.push(user.username)

    // pick out the whitelisted keys
    var sphere = _.extend(_.pick(sphereAttributes, 'name', 'company', 'url', 'domain', 'users', 'guests'), {
      userId: user._id,
      created_by: user.username,
      created_date: new Date().getTime(),
      managers: _union(interns, managers),
    });

    var sphereId = Spheres.insert(sphere);

    return sphereId;    
  }
});