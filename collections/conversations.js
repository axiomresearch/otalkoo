Conversations = new Meteor.Collection('conversations');

Conversations.allow({
  update: ownsDocument,
  remove: ownsDocument
});

Conversations.deny({
  update: function(userId, conversation, fieldNames) {
    // May only edit the following two fields:
    if (conversation.is_private)
      return (_.without(fieldNames, 'name', 'is_private', 'users', 'tags').length > 0);
    else
      return (_.without(fieldNames, 'name', 'is_private', 'tags').length > 0);
  }
});

Meteor.methods({
  createConversation: function(conversationAttributes) {
    var user = Meteor.user(),
      conversationWithSameName = Conversations.findOne({name: conversationAttributes.name});

    // Ensure the user is logged in
    if (!user)
      throw new Meteor.Error(401, "You need to login to create new conversations");

    // Ensure the conversation has a roomId
    if (!conversationAttributes.roomId)
      throw new Meteor.Error(422, 'Conversation required to be attached to a room');

    // Ensure the conversation has a parent privacy status
    // (Error message is explicitly false)
    if (!conversationAttributes.parent_is_private)
      throw new Meteor.Error(422, 'Conversation required to be attached to a room');

    // Ensure the conversation has a name
    if (!conversationAttributes.name)
      throw new Meteor.Error(422, 'Please fill in a name for the conversation');

    // Check that there are no previous conversations with the same name
    if (conversationAttributes.name && conversationWithSameName) { 
      throw new Meteor.Error(302,
'This name has already been used',
conversationWithSameName._id); }

    // Add owner as first user
    var connected = []
    connected.push(user.username)

    // pick out the whitelisted keys
    var conversation = _.extend(_.pick(conversationAttributes, 'roomId', 'parent_is_private', 'name', 'is_private', 'tags'), {
      userId: user._id,
      created_by: user.username,
      created_date: new Date().getTime(),
      users: _.union(conversationAttributes.users, connected)
    });

    var conversationId = Conversations.insert(conversation);

    return conversationId;    
  }
});