Posts = new Meteor.Collection('posts')

Posts.allow({
  insert: function(userId) {
    return userId !== undefined;
  },
  update: ownsDocument,
});

Posts.deny({
  update: function(userId, post, fieldNames) {
    // May only edit the following two fields:
    return (_.without(fieldNames, 'conversationId', 'message').length > 0);
  }
});

Meteor.methods({
  sendPost: function(postAttributes) {
    var user = Meteor.user()

    // Ensure the user is logged in
    if (!user)
      throw new Meteor.Error(401, "You need to login to send a post");

    // Ensure the post content is not empty
    if (!postAttributes.message.length > 1)
      throw new Meteor.Error(422, 'Please fill in a content for the post');

    // pick out the whitelisted keys
    var post = _.extend(_.pick(postAttributes, 'conversationId', 'message'), {
      userId: user._id,
      created_by: user.username,
      created_date: new Date().getTime()
    });

    var postId = Posts.insert(post);

    // Check if author is already set as participant
    // Within the parent conversation

    return postId;   
  }
});