/*
  Room collection
*/
Rooms = new Meteor.Collection('rooms')

Rooms.allow({
  update: ownsDocument,
  remove: ownsDocument
});

Rooms.deny({
  update: function(userId, room, fieldNames) {
    // May only edit the following two fields:
    if (room.is_private)
      return (_.without(fieldNames, 'name', 'is_private', 'users').length > 0);
    else
      return (_.without(fieldNames, 'name', 'is_private').length > 0);
  }
});

Meteor.methods({
  createRoom: function(roomAttributes) {
    var user = Meteor.user(),
      roomWithSameName = Rooms.findOne({name: roomAttributes.name});

    // Ensure the user is logged in
    if (!user)
      throw new Meteor.Error(401, "You need to login to create new rooms");

    // Ensure the room has a name
    if (!roomAttributes.name)
      throw new Meteor.Error(422, 'Please fill in a name for the room');

    // Check that there are no previous rooms with the same name
    if (roomAttributes.name && roomWithSameName) { 
      throw new Meteor.Error(302,
'This name has already been used',
roomWithSameName._id); }

    // Add owner as first user if room is private
    connected = []
    connected.push(user.username)

    if (roomAttributes.is_private && roomAttributes.users !== null)
      connected = _.union(connected, roomAttributes.users)

    // pick out the whitelisted keys
    var room = _.extend(_.pick(roomAttributes, 'name', 'is_private'), {
      userId: user._id,
      created_by: user.username,
      created_date: new Date().getTime(),
      users: connected
    });

    var roomId = Rooms.insert(room);

    return roomId;    
  }
});