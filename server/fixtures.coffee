###
if Rooms.find().count() is 0
  myRoomId = Rooms.insert(
    name: 'Public room 1',
    created_by: 'Romain FONCIER',
    created_date: new Date().getTime(),
    is_private: false,
    users: ['dMNDxWrmX4sf7GibT']
  )

  Rooms.insert(
    name: 'Public room 2',
    created_by: 'Romain FONCIER',
    created_date: new Date().getTime(),
    is_private: false,
    users: ['dMNDxWrmX4sf7GibT']
  )

  Rooms.insert(
    name: 'Private room 1',
    created_by: 'Romain FONCIER',
    created_date: new Date().getTime(),
    is_private: true,
    users: ['dMNDxWrmX4sf7GibT']
  )

  Conversations.insert(
    roomId: myRoomId,
    name: 'My first conversation',
    created_by: 'Romain FONCIER',
    created_date: new Date().getTime(),
    users: ['dMNDxWrmX4sf7GibT']
  )
###