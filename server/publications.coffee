########################
# Spheres publications #
########################

Meteor.publish 'sphere', () ->
  user = Meteor.users.findOne({_id: @.userId})
  Spheres.find({$or: [{managers: user.username}, {users: user.username}, {guests: user.username}]}) if user isnt undefined

######################
# Rooms publications #
######################
Meteor.publish 'publicRooms', () -> 
  Rooms.find({is_private: false})

Meteor.publish 'privateRoom', () ->
  user = Meteor.users.findOne({_id: @.userId})
  Rooms.find({is_private: true, users: user.username}) if user isnt undefined

# Improve this subscription :
# If public -> publish
# If private && user has access -> publish
Meteor.publish 'roomDetails', (roomId) ->
  Rooms.find({_id: roomId})

##############################
# Conversations publications #
##############################
Meteor.publish 'conversations', (roomId) -> 
  user = Meteor.users.findOne({_id: @.userId})
  Conversations.find({roomId: roomId, $or: [{is_private: false}, {is_private: true, users: user.username}]}) if user isnt undefined

Meteor.publish 'conversationDetails', (conversationId) -> 
  Conversations.find({_id: conversationId})

######################
# Posts publications #
######################
Meteor.publish 'posts', (conversationId) -> 
  Posts.find({conversationId: conversationId})

######################
# Users publications #
######################
# Publications for users who can be invited to join
# a room or a conversation.
Meteor.publish 'usersToInvit', ->
  Meteor.users.find({},
    fields:
      username: 1
  )